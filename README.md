# estudiotouch

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# WishList

* Strip non-digit \[^A-Za-z0-9\] from the search field. (SECURITY)
* There's a problem with Vue Router that the component is not update, so have to refresh the page.
* Put bootstrap inside Vue.
