import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Search from './Search.vue'
import NotFound from './NotFound.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes: [
    { path: '/', name: 'App', component: App },
    { path: '/search/:username', component: Search, name: 'Search' },
    { path: '*', name: 'NotFound', component:  NotFound }
  ]
})
