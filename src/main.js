import App from './App.vue'
import Search from './Search.vue'
import Vue from 'vue'
import VueResource from 'vue-resource'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"

Vue.use(VueResource)


new Vue({
  router,
  template: `
    <div id="app">
      <router-view></router-view>
    </div>
  `
}).$mount('#app')
